package com.tw.BookShop;

import org.junit.Assert;
import org.junit.Test;

public class RestControllerTest {

    @Test
    public void shouldReturnBookShopObject() {
        RestController restController = new RestController();
        Assert.assertEquals(BookShop.class, restController.welcome().getClass());
    }
}
