package com.tw.BookShop;

import org.junit.Assert;
import org.junit.Test;

public class BookShopTest {

    @Test
    public void shouldReturnTitle() {
        BookShop bookShop = new BookShop();
        Assert.assertEquals("Book Shop", bookShop.getName());
    }
}
