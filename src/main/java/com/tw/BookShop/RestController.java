package com.tw.BookShop;

import org.springframework.web.bind.annotation.RequestMapping;

@org.springframework.web.bind.annotation.RestController
public class RestController {

    @RequestMapping("/application")
    public BookShop welcome() {
        return new BookShop();
    }
}
