FROM openjdk:8-alpine
ADD /build/libs/BookShop-0.0.1-SNAPSHOT.jar bookshop.jar
ENTRYPOINT ["java","-jar","/bookshop.jar"]